/*
ESA LAF

Copyright © 2016 Ivan Katanic

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <algorithm>
#include <cassert>
#include <cstring>
#include <iostream>
#include <fstream>
#include <stack>

using namespace std;

#include "sais.cpp"

#define SetFlag(s, x) s |= x
#define UnsetFlag(s, x) s &= ~x
#define TestFlag(s, x) (s & x)

enum Flags {
  Active = 1 << 0,
  Dead = 1 << 1,
  InPq = 1 << 2,
  SamePositionsAsParent = 1 << 3,
};


struct Character {
  int ch;
  int prev;
  int next;
  bool dead;
};
// linked-list representing current state of the string
vector<Character> s;


class Position { // 4 bytes
  int p;
public:
  Position(int _p = 0) : p(_p << 2) {};
  inline int pos() { return (p>>2); }
  inline int& flags() { return p; }
};


struct Node { // 56 bytes
  vector<Position> positions; // suffixes sorted by starting position

  int len; // original lcp (i.e. interval's lcp over the original string)
  int lcp; // decreases over time, interval's lcp over the current string 
  int parent;
  int cntActive;
  int pqWeight; 
  int posInPq;
  int lastRuleUpdate;
  char flags;

  inline int weight() { return cntActive * lcp; }
};

vector<Node> nodes;

vector<int> SA, lcp;
vector<int> TerminalNode; // leaf id for every suffix

vector<vector<int>> pq;

vector<int> invPosStorage; // all inverse position laid out
vector<int*> InversePositions; // Inverse positions matrix

string text;
int len;

ofstream out;

void initSA() {
  unsigned char* tmp = new unsigned char[len + 1];
  for (int i = 0; i < len; ++i)
    tmp[i] = text[i];
  tmp[len] = 0;

  SA.resize(len + 1);
  SA_IS(tmp, &SA[0], len + 1, 255, 1);
  assert(SA[0] == len);

  vector<int> invSA(len);
  for (int i = 0; i < len; ++i) {
    SA[i] = SA[i + 1];
    invSA[SA[i]] = i;
  }
  SA.pop_back();

  lcp.resize(len);
  lcp[0] = 0;
  int h = 0;
  for (int i = 0; i < len; ++i) {
    if (invSA[i] > 0) {
      int j = SA[ invSA[i]-1 ];
      while (i+h < len && j+h < len && tmp[i+h] == tmp[j+h]) h++;
      lcp[invSA[i]] = h;
      if (h > 0) --h;
    }
  }
  
  delete []tmp;
}

void initLcpTree() {
  nodes.push_back({});
  TerminalNode.resize(len);

  stack<int> S;
  S.push(0);
  
  lcp.push_back(0);
  for (int i = 1; i <= len; ++i) {
    if (lcp[i-1] > lcp[i]) TerminalNode[SA[i-1]] = S.top();

    int last = -1;
    while (nodes[S.top()].lcp > lcp[i]) {
      if (last != -1) nodes[last].parent = S.top();
      last = S.top();
      S.pop();
    }

    if (nodes[S.top()].lcp < lcp[i]) {
      int idx = nodes.size();
      nodes.push_back({});
      nodes[idx].lcp = nodes[idx].len = lcp[i];
      S.push(idx);
    }

    if (last != -1) nodes[last].parent = S.top();
    if (lcp[i-1] <= lcp[i]) TerminalNode[SA[i-1]] = S.top();
  }
  lcp.pop_back();

  S.pop();

  lcp.clear();
  SA.clear();
  lcp.shrink_to_fit();
  SA.shrink_to_fit();
}

void calcActivePositions(int x) {
  nodes[x].cntActive = 0;
  int lastActive = 0;
  for (Position& pos: nodes[x].positions) {
    if (!TestFlag(pos.flags(), Dead) && (lastActive <= pos.pos())) {
      SetFlag(pos.flags(), Active);
      lastActive = pos.pos() + nodes[x].len;
      nodes[x].cntActive++;
    } else {
      UnsetFlag(pos.flags(), Active);
    }
  }
}

void initNodePositions() {
  unsigned int positionsCnt = 0;
  for (int i = 0; i < len; ++i) {
    int x = TerminalNode[i];
    while (x) {
      if (nodes[x].lcp > 1) positionsCnt++;
      x = nodes[x].parent;
    }
  }

  invPosStorage.reserve(positionsCnt);

  InversePositions.resize(len);
  for (int i = 0; i < len; ++i) {
    InversePositions[i] = &invPosStorage.back() + 1;
    int x = TerminalNode[i];
    while (x) {
      if (nodes[x].lcp > 1) {
        invPosStorage.push_back(nodes[x].positions.size());
        nodes[x].positions.push_back(Position(i));
      }
      x = nodes[x].parent;
    }
  }
  
  for (int i = 0; i < (int)nodes.size(); ++i)
    calcActivePositions(i);
}

void addToPq(int x) {
  assert(!TestFlag(nodes[x].flags, InPq));
  nodes[x].pqWeight = nodes[x].weight();
  if (nodes[x].cntActive <= 1 || nodes[x].lcp <= 1) return;
  SetFlag(nodes[x].flags, InPq);
  nodes[x].posInPq = pq[nodes[x].pqWeight].size();
  pq[nodes[x].pqWeight].push_back(x);
}

void removeFromPq(int x) {
  assert(TestFlag(nodes[x].flags, InPq));
  UnsetFlag(nodes[x].flags, InPq);
  auto& v = pq[nodes[x].pqWeight];
  nodes[v.back()].posInPq = nodes[x].posInPq;
  swap(v.back(), v[nodes[x].posInPq]);
  v.pop_back();
}

void updatePqWeight(int x) {
  if (TestFlag(nodes[x].flags, InPq)) {
    if (nodes[x].weight() == nodes[x].pqWeight) return;
    removeFromPq(x);
  }
  addToPq(x);
}

void initPq() {
  int maxw = 0;
  for (Node& x: nodes)
    if (x.lcp > 1) maxw = max(maxw, x.lcp * x.cntActive);
  pq.resize(maxw + 1);
  for (int x = 0; x < (int)nodes.size(); ++x) {
    addToPq(x);
    if (nodes[x].lcp <= 1) SetFlag(nodes[x].flags, Dead);
  }
}


vector<pair<int, int>> positionsToFix;

void deactivatePosition(int x, int p) {
  if (TestFlag(nodes[x].positions[p].flags(), Dead)) return;
  SetFlag(nodes[x].positions[p].flags(), Dead);

  if (TestFlag(nodes[x].positions[p].flags(), Active)) {  
    positionsToFix.push_back({x, p});
  }
}

void fixActivePosition(int x, int p) {
  if (!TestFlag(nodes[x].positions[p].flags(), Active)) return;
  
  UnsetFlag(nodes[x].positions[p].flags(), Active);
  nodes[x].cntActive--;
  int lastActive = 0;
  
  while (++p < (int)nodes[x].positions.size()) {
    if (TestFlag(nodes[x].positions[p].flags(), Dead)) {
      if (TestFlag(nodes[x].positions[p].flags(), Active)) {
        UnsetFlag(nodes[x].positions[p].flags(), Active);
        nodes[x].cntActive--;
      }
      continue;
    }
    
    if (lastActive <= nodes[x].positions[p].pos()) { // no overlap
      if (TestFlag(nodes[x].positions[p].flags(), Active)) break;
      SetFlag(nodes[x].positions[p].flags(), Active);
      nodes[x].cntActive++;
      lastActive = nodes[x].positions[p].pos() + nodes[x].len;
    } else { // overlap
      if (TestFlag(nodes[x].positions[p].flags(), Active)) { // deactivate
        UnsetFlag(nodes[x].positions[p].flags(), Active);
        nodes[x].cntActive--;
      }
    }
  } 
}

int insertNewNode(int x, int nlen, int nlcp) {
  int idx = nodes.size();
  nodes.push_back({});
  nodes[idx].len = nlen;
  nodes[idx].lcp = nlcp;
  nodes[idx].parent = nodes[x].parent;
  nodes[idx].positions = nodes[x].positions;
  if (TestFlag(nodes[x].flags, SamePositionsAsParent))
    SetFlag(nodes[idx].flags, SamePositionsAsParent);
  
  nodes[x].parent = idx;
  SetFlag(nodes[x].flags, SamePositionsAsParent);
  
  for (Position& pos: nodes[idx].positions) {
    if ((s[pos.pos()].dead) || 
        (pos.pos() + nlen < len && s[pos.pos() + nlen].dead)) SetFlag(pos.flags(), Dead);
    else UnsetFlag(pos.flags(), Dead);
  }

  calcActivePositions(idx);
  addToPq(idx);
  return idx;
}

vector<int> killedByPosIdx;
vector<int> killedSoFar;

void replaceSubstring(vector<int> &pos, int ruleLen, int ruleLcp, int ruleIdx) {
  // We replace all substrings of 'ruleLcp' characters starting at positions from list 'pos'.
  // All of those substrings cover 'ruleLen' characters from original string, some of them
  // possibly dead (actually, exactly ruleLen-ruleLcp of them are dead).
  // We call each of those substrings an 'instance'.
  // Here we go through instances from right to left and update the data structures accordingly.
  // For each instance we first kill all the characters (and their positions) but the first one.
  // Those characters are called 'right neighbourhood' of an instance.
  // First character is being replaced by the character denoting the new rule.
  // After that we update characters to the left of the current instance, including the first
  // character of the instance. We limit ourselves to characters which are between current and
  // next instance (i.e. first instance to the left).
  // We call those characters 'left neighbourhood' of the current instance.
  
  for (int posIdx = (int)pos.size() - 1; posIdx >= 0; --posIdx) {
    int a = pos[posIdx];
    int b = pos[posIdx] + ruleLen;
    
    assert(!s[a].dead);
    s[a].ch = ruleIdx;

    // right neighbourhood
    int cntKilled = 0;
    int lastAlive = a;
    for (int i = s[a].next; i < b; i = s[i].next) {
      killedByPosIdx[i] = posIdx;
      killedSoFar[i] = ++cntKilled;
      lastAlive = i;
    }

    for (int i = lastAlive; i > a; i = s[i].prev) {
      int x = TerminalNode[i], j = 0;
      while (x) {
        if (!TestFlag(nodes[x].flags, Dead)) deactivatePosition(x, InversePositions[i][j]);
        if (!TestFlag(nodes[x].flags, SamePositionsAsParent)) j++;
        x = nodes[x].parent;
      }
      s[i].dead = true;
      s[i].ch = ruleIdx;
      if (s[i].prev != -1) s[ s[i].prev ].next = s[i].next;
      if (s[i].next != len) s[ s[i].next ].prev = s[i].prev;
    }

    int lo = posIdx == 0 ? 0 : pos[posIdx-1] + ruleLen;
    // left neighbourhood
    for (int i = a; i >= lo; i = s[i].prev) {
      int x = TerminalNode[i];
      if (i + nodes[x].len <= a) break;
      
      int j = 0;
      while (x && i + nodes[x].len > a) {
        if (!TestFlag(nodes[x].flags, Dead)) {
          /* Here we have two possible cases for a position from the left neighbourhood:
                  1.) it intersects with some instance and doesn't contain it completely
                  2.) it only contains some instances completely
          */
          
          if (s[i + nodes[x].len].dead && s[i + nodes[x].len].ch == ruleIdx) {
            // 1st case

            // Kill the position because it's no longer valid
            if (!TestFlag(nodes[x].positions[ InversePositions[i][j] ].flags(), Dead)) {
              deactivatePosition(x, InversePositions[i][j]);
            }

            // Check for a possible new interval
            int idx = killedByPosIdx[i + nodes[x].len]; // ID of the instance we are intersecting.
            int start = pos[idx]; // where the intersecting instance starts?

            // If parent doesn't overlap with the instance, insert a new node inbetween x and the parent.
            if (i + nodes[ nodes[x].parent ].len < start) {
              // Calculate it's lcp by taking lcp of x and subtracting suffix that's intersecting
              // with the instance.
              int newLcp = nodes[x].lcp - killedSoFar[i + nodes[x].len];
              if (nodes[x].lastRuleUpdate != ruleIdx) {
                // Subtract the instances completely inside the substring.
                // All of them are now ruleLcp-1 characters shorter.
                newLcp -= (idx - posIdx) * (ruleLcp - 1);
              }

              if (newLcp > 1) {
                int idx = insertNewNode(x, start - i, newLcp);
                nodes[idx].lastRuleUpdate = ruleIdx;
                // node.lastRuleUpdate set to ruleIdx means that the node is aware
                // of the current rule instances that are completely inside of it.
                // That is, it's lcp counts all instances as being 1 character long.
              }
            }
          } else {
            // 2nd case

            // If this position is alive, subtract instances completely inside the substring,
            // but only if that hasn't already been done for that interval.
            if (!TestFlag(nodes[x].positions[ InversePositions[i][j] ].flags(), Dead) && nodes[x].lastRuleUpdate != ruleIdx) {
              int cnt = upper_bound(pos.begin() + posIdx, pos.end(), i + nodes[x].len - ruleLen) - pos.begin() - posIdx;
              nodes[x].lcp -= cnt * (ruleLcp - 1);
              nodes[x].lastRuleUpdate = ruleIdx;
              updatePqWeight(x);
            }
          }
        }
        
        if (!TestFlag(nodes[x].flags, SamePositionsAsParent)) j++;
        x = nodes[x].parent;
      }
    }
  }

  // First we marked as dead all the positions that need to be deactivated.
  // Now we go through all of them and deactivate them.
  for (int i = (int)positionsToFix.size() - 1; i >= 0; --i)
    fixActivePosition(positionsToFix[i].first, positionsToFix[i].second);
  for (auto& p: positionsToFix) 
    updatePqWeight(p.first);
  positionsToFix.clear();
}

string getSubstring(int a, int b) {
  string ret = "";
  for (int i = a; i < b; i = s[i].next) {
    if (s[i].next != i + 1) ret += "*" + to_string(s[i].ch) + "*";
    else ret.push_back((char)s[i].ch);
  }
  return ret;
}

void laf() {
  killedByPosIdx.resize(len+1);
  killedSoFar.resize(len+1);

  int rulesCnt = 0;
  for (int w = (int)pq.size() - 1; w >= 0; --w) {
    while (pq[w].size()) {
      int i = pq[w].back();
      removeFromPq(i);
      SetFlag(nodes[i].flags, Dead);

      int len = nodes[i].len;
      int lcp = nodes[i].lcp;
      rulesCnt++;

      vector<int> v;
      for (Position& pos: nodes[i].positions)
        if (TestFlag(pos.flags(), Active)) v.push_back(pos.pos());

      out << "W = " << w << ": " << getSubstring(v[0], v[0] + len);
      for (int p: v) out << ", " << p;
      out << endl;

      replaceSubstring(v, len, lcp, rulesCnt);

      nodes[i].positions.clear();
      nodes[i].positions.shrink_to_fit();
    }
    pq[w].shrink_to_fit();
  }
}

char* read(string infile) {
  ifstream in(infile, ios::ate);
  if (!in.is_open()) {
    printf("Cannot open %s.\n", infile.c_str());
    exit(1);
  }

  int size = in.tellg();
  in.seekg(0, ios::beg);
  char* memblock = new char[size + 1];
  in.read(memblock, size-1); // skip newline
  in.close();
  return memblock;
}

int main(int argc, char** argv) {
  if (argc != 3) {
    puts("Usage: ./main input_file output_file.");
    return 1;
  }

  text = read(argv[1]); 
  len = text.size();
  s.resize(len);
  for (int i = 0; i < len; ++i)
    s[i] = {text[i], i-1, i+1, false};
  
  out.open(argv[2]);

  initSA();
  initLcpTree();
  initNodePositions();
  initPq();
  laf();

  for (int i = 0; i < len; ++i)
    if (s[i].dead || (i + 1 < len && s[i+1].dead)) text[i] = '.';

  out << text << endl;
  return 0;
}

