ESA LAF is a C++11 implementation of the largest area first parsing of a string algorithm described in the paper:

Strahil Ristov and Ivan Katanic
"Largest area first parsing of a string is fast on real data"

in the process of publishing.



The source code should be compiled using g++ with -std=c++0x -O2 options.

Usage is:	./main input output

Output consists of rules in order of their corresponding area sizes, expressed as the area size, rule definition, and the list of positions of the rule in the string. At the end of the output is a copy of the input string with only the characters that are not included in any rule. 

sais.cpp is a source code for an efficient suffix array construction by Ge Nong, Sen Zhang and Wai Hong Chan, described in paper "Two efficient algorithms for linear time suffix array construction" IEEE Trans. Computers 60(10) (2011), originally downloaded from https://code.google.com/archive/p/ge-nong/downloads. Thanks.